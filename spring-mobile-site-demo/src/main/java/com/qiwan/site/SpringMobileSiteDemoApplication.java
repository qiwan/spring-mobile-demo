package com.qiwan.site;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMobileSiteDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMobileSiteDemoApplication.class, args);
	}

}