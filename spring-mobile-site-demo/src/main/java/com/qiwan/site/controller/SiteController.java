package com.qiwan.site.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.site.SitePreference;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class SiteController {
	
	private static final Logger log = LoggerFactory.getLogger(SiteController.class);
	
	@RequestMapping("site")
	public String site(SitePreference sitePreference) {
		if (sitePreference == SitePreference.MOBILE) {
			log.info("========请求来源设备是手机！========");
			return "mobile/index";
		} else if (sitePreference == SitePreference.TABLET) {
			log.info("========请求来源设备是平板！========");
			return "tablet/index";
		} else {
			log.info("========请求来源设备是其它！========");
			return "index";
		}
	}
}
