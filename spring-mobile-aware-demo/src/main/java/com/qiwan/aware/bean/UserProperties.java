package com.qiwan.aware.bean;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "custom.user")
public class UserProperties {
	
	private String name;
	
	private Integer age;
	
	private String sex;
}
