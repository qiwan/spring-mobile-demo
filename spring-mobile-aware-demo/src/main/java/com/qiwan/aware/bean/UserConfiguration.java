package com.qiwan.aware.bean;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data
@Configuration
@EnableConfigurationProperties(UserProperties.class)
public class UserConfiguration {
	
	private UserProperties userProperties;

}
