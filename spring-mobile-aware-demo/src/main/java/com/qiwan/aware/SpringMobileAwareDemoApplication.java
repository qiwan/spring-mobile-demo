package com.qiwan.aware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMobileAwareDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMobileAwareDemoApplication.class, args);
	}

}