package com.qiwan.aware.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class AwareController {
	
	@RequestMapping("aware")
	public String aware() {
		return "index";
	}
}
