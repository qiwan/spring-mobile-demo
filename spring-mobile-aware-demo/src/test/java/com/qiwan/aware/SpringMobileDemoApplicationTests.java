package com.qiwan.aware;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.qiwan.aware.bean.UserConfiguration;
import com.qiwan.aware.bean.UserProperties;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
class SpringMobileDemoApplicationTests {
	
	@Autowired
	private UserProperties user;
	
	@Autowired
	private UserConfiguration userConfiguration;

	@Test
	public void contextLoads() {
		log.info("User：{}",user);
	}
	
	@Test
	public void test1() {
		UserProperties userProperties = userConfiguration.getUserProperties();
		log.info("userProperties：{}", userProperties);
	}

}
