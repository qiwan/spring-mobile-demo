package com.qiwan.device;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringMobileDeviceDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMobileDeviceDemoApplication.class, args);
	}

}