package com.qiwan.device.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class DeviceController {
	
	private static final Logger log = LoggerFactory.getLogger(DeviceController.class);
	
	@RequestMapping("device")
	public String device(Device device) {
		if (device.isMobile()) {
			log.info("========请求来源设备是手机！========");
			return "mobile/index";
		} else if (device.isTablet()) {
			log.info("========请求来源设备是平板！========");
			return "tablet/index";
		} else {
			log.info("========请求来源设备是其它！========");
			return "index";
		}
	}
}
